
# Changelog for workspace

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v2.4.2] - 2023-03-30

Ported to git
see #24836

## [v2.4.0] - 2018-01-25

Ported to Java 8

Added two more constants needed for questionnaire urls

## [v2.3.0] - 2017-05-12

Added support for email plugin multi part (text and html) handling

Added support for email templates

Added support for remove from scope

Added support for ASLSession methods replacement		

## [v1.1.0] - 2015-07-08

Added method for getting portal email sender (from)

Added method for getting portal instance name

## [v1.0.0] - 2013-10-21

First release 
